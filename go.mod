module gitlab.com/cabby365/x

go 1.15

require (
	github.com/labstack/echo/v4 v4.2.1 // indirect
	github.com/okta/okta-jwt-verifier-golang v1.1.1 // indirect
)
