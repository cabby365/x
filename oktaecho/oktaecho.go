package oktaecho

import (
	"net/http"

	"github.com/labstack/echo/v4"
	jwtverifier "github.com/okta/okta-jwt-verifier-golang"
)

const (
	Authorization_Header = "Authorization"
	Context_JWT_Token    = "JWT_TOKEN"
)

type OEcho struct {
	verifier *jwtverifier.JwtVerifier
}

func ProvideOEcho(verifier jwtverifier.JwtVerifier) OEcho {
	return OEcho{
		verifier: verifier.New(),
	}
}

func (oe OEcho) Middleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token, err := oe.verifier.VerifyAccessToken(c.Request().Header.Get(Authorization_Header))
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, "Invalid access token")
		}
		c.Set(Context_JWT_Token, token.Claims)
		return next(c)
	}
}
